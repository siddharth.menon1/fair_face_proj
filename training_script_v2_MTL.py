import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import tensorflow as tf
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.models import Model
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Lambda
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Input
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint
import wandb
from wandb.keras import WandbCallback


import cv2
import numpy as np
import matplotlib.pyplot as plt
import PIL
from skimage import color
from skimage import io
from skimage.transform import resize
import time
import pickle

config = {
              "learning_rate": 0.0001,
              "epochs": 1,
              "batch_size": 32,
            #   "log_step": 200,
              "architecture": "MTL",
              "dataset": "FairFace"
           }

def parse_image(filename):
  image = tf.io.read_file("/mnt/siddharth/fair_face/"+filename)
  image = tf.image.decode_jpeg(image, channels=3)
  image = tf.image.resize(image, [224, 224])
  image = tf.image.per_image_standardization(image)
  return image

def loading_data(path_to_images, train_or_test="test"):
  if train_or_test == "train":
    no_of_data = 86744
  else:
    no_of_data = 10954

  with open(path_to_images, 'rb') as f:
    labels = pickle.load(f)
  filenames, age_labels, gender_labels, race_labels = [], [], [], []
  for i in range(0, no_of_data):
    filenames.append(labels[i][0])
    age_labels.append(labels[i][1])
    gender_labels.append(labels[i][2])
    race_labels.append(labels[i][3])

  filenames_ds = tf.data.Dataset.from_tensor_slices(filenames)
  images_ds = filenames_ds.map(parse_image, num_parallel_calls=tf.data.experimental.AUTOTUNE)

  labels_ds = tf.data.Dataset.from_tensor_slices((age_labels, race_labels, gender_labels))
  dataset = tf.data.Dataset.zip((images_ds, labels_ds)).batch(1)

  return dataset


class MultiTaskModel():
    def make_default_hidden_layers(self, inputs):
        x = Conv2D(16, (3, 3), padding="same")(inputs)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=-1)(x)
        x = MaxPooling2D(pool_size=(3, 3))(x)
        x = Dropout(0.25)(x)
        x = Conv2D(32, (3, 3), padding="same")(x)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=-1)(x)
        x = MaxPooling2D(pool_size=(2, 2))(x)
        x = Dropout(0.25)(x)
        x = Conv2D(32, (3, 3), padding="same")(x)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=-1)(x)
        x = MaxPooling2D(pool_size=(2, 2))(x)
        x = Dropout(0.25)(x)
        return x

    def build_race_branch(self, inputs, num_races):
        x = self.make_default_hidden_layers(inputs)
        x = Flatten()(x)
        x = Dense(128)(x)
        x = Activation("relu")(x)
        x = BatchNormalization()(x)
        x = Dropout(0.5)(x)
        x = Dense(num_races)(x)
        x = Activation("softmax", name="race_output")(x)
        return x

    def build_gender_branch(self, inputs):
        x = Lambda(lambda c: tf.image.rgb_to_grayscale(c))(inputs)
        x = self.make_default_hidden_layers(inputs)
        x = Flatten()(x)
        x = Dense(128)(x)
        x = Activation("relu")(x)
        x = BatchNormalization()(x)
        x = Dropout(0.5)(x)
        x = Dense(1)(x)
        x = Activation("sigmoid", name="gender_output")(x)
        return x

    def build_age_branch(self, inputs, num_age_groups):
        x = self.make_default_hidden_layers(inputs)
        x = Flatten()(x)
        x = Dense(128)(x)
        x = Activation("relu")(x)
        x = BatchNormalization()(x)
        x = Dropout(0.5)(x)
        x = Dense(num_age_groups)(x)
        x = Activation("softmax", name="age_output")(x)
        return x

    def assemble_full_model(self, width, height, num_age_groups, num_races):
        input_shape = (height, width, 3)
        inputs = Input(shape=input_shape)
        age_branch = self.build_age_branch(inputs, num_age_groups)
        race_branch = self.build_race_branch(inputs, num_races)
        gender_branch = self.build_gender_branch(inputs)
        model = Model(inputs=inputs,
                     outputs = [age_branch, race_branch, gender_branch],
                     name="face_net")
        return model
    

if __name__ == "__main__":
    wandb.init(project="fair_face_model_v1", entity="sinister")
    run = wandb.init(project='my-tf-integration', config=config)
    config = wandb.config
    train_model_optimizer = tf.keras.optimizers.Adam(1e-4)

    train_dataset= loading_data("/mnt/siddharth/fair_face/labels.pkl", "train")
    val_dataset= loading_data("/mnt/siddharth/fair_face/labels_val.pkl")
    train_model = MultiTaskModel().assemble_full_model(224, 224, num_age_groups=9, num_races=7)
    train_model.compile(optimizer=train_model_optimizer, 
              loss={
                  'age_output': 'categorical_crossentropy', 
                  'race_output': 'categorical_crossentropy', 
                  'gender_output': 'binary_crossentropy'},
              loss_weights={
                  'age_output': 1.0, 
                  'race_output': 1.0, 
                  'gender_output': 1.0},
              metrics={
                  'age_output': 'categorical_accuracy', 
                  'race_output': 'categorical_accuracy',
                  'gender_output': 'binary_accuracy'})
    batch_size = 32
    history = train_model.fit(train_dataset,
                        steps_per_epoch=86744//batch_size,
                        epochs=30,
                        callbacks=[WandbCallback()],
                        validation_data=val_dataset,
                        validation_steps=10954//batch_size)
    run.finish()

    
    train_model.save(f"ustom_cov_3branch_model/model_{time.time()}.model")
    
    


