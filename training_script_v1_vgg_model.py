import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import tensorflow as tf
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Conv2D, AveragePooling2D, UpSampling2D, Input, Flatten, Dense, Reshape, BatchNormalization, LeakyReLU
from tensorflow.keras.optimizers import Adam
import wandb


import cv2
import numpy as np
import matplotlib.pyplot as plt
import PIL
from skimage import color
from skimage import io
from skimage.transform import resize
import time
import pickle

config = {
              "learning_rate": 0.001,
              "epochs": 1,
              "batch_size": 64,
              "log_step": 200,
              "architecture": "VGG+Dense1024",
              "dataset": "FairFace"
           }

def parse_image(filename):
  image = tf.io.read_file("/mnt/siddharth/fair_face/"+filename)
  image = tf.image.decode_jpeg(image, channels=3)
  image = tf.image.resize(image, [224, 224])
  image = tf.image.per_image_standardization(image)
  return image

def loading_train_data():

  with open('/mnt/siddharth/fair_face/labels.pkl', 'rb') as f:
    labels = pickle.load(f)
  filenames, age_labels, gender_label, race_label = [], [], [], []
  for i in range(0, 86744):
    filenames.append(labels[i][0])
    age_labels.append(labels[i][1])
    gender_label.append(labels[i][2])
    race_label.append(labels[i][3])

  filenames_ds = tf.data.Dataset.from_tensor_slices(filenames)
  images_ds = filenames_ds.map(parse_image, num_parallel_calls=tf.data.experimental.AUTOTUNE).batch(64)

  age_labels_ds = tf.data.Dataset.from_tensor_slices(age_labels).batch(64)
  gender_labels_ds = tf.data.Dataset.from_tensor_slices(gender_label).batch(64)
  race_labels_ds = tf.data.Dataset.from_tensor_slices(race_label).batch(64)
  train_dataset = zip(images_ds, age_labels_ds, gender_labels_ds, race_labels_ds)

  return train_dataset


def vgg_global_style(layer_name):
  vgg = tf.keras.applications.VGG19(include_top=True, weights=None)
  vgg.load_weights('/mnt/siddharth/fair_face/vgg19_weights_tf_dim_ordering_tf_kernels.h5')
  vgg.trainable = False
  outputs = vgg.get_layer(layer_name).output
  model = tf.keras.Model([vgg.input], outputs)
  return model

vgg_model = vgg_global_style('block5_pool')
# print(vgg_model.output_shape)

def model_v1(image_shape=(224, 224, 3)):
  inputs = Input(image_shape)
  vgg_output = vgg_model(inputs)
  flatten_vgg_output = Flatten()(vgg_output)
  # vgg_output = tf.expand_dims(vgg_output, axis = 0)
  dense3 = Dense(1024, kernel_initializer="he_normal", name="dense3", activation="relu")(flatten_vgg_output)
  dense3 = Dense(512, kernel_initializer="he_normal", name="dense3", activation="relu")(flatten_vgg_output)
  dense4 = Dense(256, kernel_initializer="he_normal", name="dense4", activation="relu")(dense3)
  dense5 = Dense(128, kernel_initializer="he_normal", name="dense5", activation="relu")(dense4)
  dense6 = Dense(64, kernel_initializer="he_normal", name="dense6", activation="relu" )(dense5)

  age = Dense(9, kernel_initializer="he_normal", name="age", activation='softmax')(dense6) 
  gender = Dense(1, kernel_initializer="he_normal", name="gender", activation='sigmoid')(dense6)
  race = Dense(7, kernel_initializer="he_normal", name="race", activation='softmax')(dense6) 

  model = Model([inputs], [age, gender, race])

  return model

def model_v1_loss(age_output, age_label, gender_output, gender_label, race_output, race_label):
  binary_ce = tf.keras.losses.BinaryCrossentropy()
  categorical_ce = tf.keras.losses.CategoricalCrossentropy()

  age_loss = categorical_ce(age_label, age_output)
  gender_loss = binary_ce(gender_label, gender_output)
  race_loss = categorical_ce(race_label, race_output)

  Total_Loss = 1.0*age_loss + 1.0*gender_loss + 1.0*race_loss

  return Total_Loss

def train_step(image, age_label, gender_label, race_label, model, train_acc_metric):
  ## Pre process Image
  train_model_optimizer = tf.keras.optimizers.Adam(1e-3)
  train_acc_metric_age = train_acc_metric[0]
  train_acc_metric_gender = train_acc_metric[1]
  train_acc_metric_race = train_acc_metric[2]

  with tf.GradientTape() as tape:
    # Forward Pass
    age_output, gender_output, race_output = model(image, training=True)

    total_loss = model_v1_loss(age_output, age_label, gender_output, gender_label, race_output, race_label)

  gradients_of_model = tape.gradient(total_loss, model.trainable_variables)
  train_model_optimizer.apply_gradients(zip(gradients_of_model, model.trainable_variables))

  train_acc_metric_age.update_state(age_label, age_output)
  train_acc_metric_gender.update_state(gender_label, gender_output)
  train_acc_metric_race.update_state(race_label, race_output)

  return total_loss, train_acc_metric_age, train_acc_metric_gender, train_acc_metric_race

def train(train_dataset, model, train_acc_metric, epochs=1):
  for epoch in range(epochs):
    print("\nStart of epoch %d" % (epoch,))

    # Iterate over the batches of the dataset
    for image, age_label, gender_label, race_label in train_dataset:
      train_loss = []
      loss_value, train_acc_metric_age, train_acc_metric_gender, train_acc_metric_race = train_step(image, age_label, gender_label, race_label, model, train_acc_metric)
      train_loss.append(float(loss_value))
      train_acc_age = train_acc_metric_age.result()
      train_acc_gender = train_acc_metric_gender.result()
      train_acc_race = train_acc_metric_race.result()
      wandb.log({'epochs': epoch,
                  'loss': np.mean(train_loss),
                  'age_acc': float(train_acc_age),
                  'gender_acc': float(train_acc_gender),
                  'race_acc': float(train_acc_race)})

    # print(f"Loss = {loss_value} for epoch = {epoch}")
    if epoch == 1:
      model.save(f"models_after_epochs/model_after_epoch_{epoch}.model")
    

if __name__ == "__main__":
    wandb.init(project="fair_face_model_v1", entity="sinister")
    run = wandb.init(project='my-tf-integration', config=config)
    config = wandb.config
    train_acc_metric_age = tf.keras.metrics.CategoricalAccuracy()
    train_acc_metric_gender = tf.keras.metrics.BinaryAccuracy()
    train_acc_metric_race = tf.keras.metrics.CategoricalAccuracy()
    train_acc_metric = [train_acc_metric_age, train_acc_metric_gender, train_acc_metric_race]

    train_dataset = loading_train_data()
    train_model = model_v1()
    train(train_dataset, train_model, train_acc_metric, epochs=2)


